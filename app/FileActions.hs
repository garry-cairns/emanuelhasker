module FileActions
  ( gameFromFile
  )
where

import           Data.Text.IO                   ( readFile )
import           Parsers.PGN                    ( Game(..)
                                                , parsePGN
                                                )
import           Prelude                 hiding ( readFile )
import           Text.Megaparsec                ( parse )

gameFromFile :: FilePath -> IO Game
gameFromFile fp = do
  fileText <- readFile fp
  case parse parsePGN "" fileText of
    Left s -> error (show s)
    Right game -> return game
