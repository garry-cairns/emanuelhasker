module Main where

import           ChessLib                       ( Coordinate
                                                , chessIndices
                                                )
import           Data.Text                      ( Text )
import           Graphics.Gloss
import           FileActions                    ( gameFromFile )

data Board = Board

data World = World
  { score         :: Int
  , moves         :: [Text]
  }
  deriving (Eq, Show)

colourSelect :: Coordinate -> Color
colourSelect (x, y) | snd (divMod x 2) == snd (divMod y 2) = greyN 0.1
                    | otherwise                            = greyN 0.9

displaySquare :: Float -> Color -> Picture
displaySquare rad col = color col $ rectangleSolid rad rad

displaySquares :: Float -> Picture
displaySquares size = Pictures
  [ translate (fromIntegral (fst c) * size) (fromIntegral (snd c) * size)
      $ displaySquare size (colourSelect c)
  | c <- chessIndices
  ]

window :: Display
window = InWindow "Emanuel Hasker" (600, 400) (10, 10)

background :: Color
background = greyN 0.67

displayBoard :: Picture
displayBoard = displaySquares 20

main :: IO ()
main = display window background displayBoard
