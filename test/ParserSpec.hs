{-# LANGUAGE OverloadedStrings #-}

module ParserSpec where

import           Prelude                 hiding ( unlines )
import qualified Data.Map.Strict               as Map
import           Data.Text                      ( unlines )
import           Test.Hspec
import           Test.Hspec.Megaparsec          ( shouldSucceedOn )
import           Text.Megaparsec                ( parse )
import           ChessLib
import           GameState
import           Parsers.FEN
import           Parsers.PGN

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "FENParsing" $ do

    it "Correctly parses FEN"
      $ parse parseFEN ""
      `shouldSucceedOn` "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

--    it "Correctly describes the side to move from a FEN showing a new game"
--      $          sideToMove
--                   (parseFEN "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
--      `shouldBe` White
--
--    it "Correctly describes the available castles from a FEN showing a new game"
--      $          availableCastles
--                   (parsedFEN "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
--      `shouldBe` [ Kingside White
--                 , Queenside White
--                 , Kingside Black
--                 , Queenside Black
--                 ]
--
--    it
--        "Correctly describes en passant square from a FEN showing an appropriate pawn move"
--      $          enPassant
--                   (parsedFEN
--                     "rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR b KQkq d3 0 1"
--                   )
--      `shouldBe` "d3"
--
--    it "Correctly parses and unparses positions from FEN notaion"
--      $          boardToFENString
--                   (position
--                     (parsedFEN
--                       "rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR b KQkq d3 0 1"
--                     )
--                   )
--      `shouldBe` "rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR"
--
  describe "PGNParsing" $ do

    it "Correctly parses PGN" $ parse parsePGN "" `shouldSucceedOn` pgnString

--    it "Correctly parses events from PGN notation"
--      $          event (fst . head $ parsedPGN pgnString)
--      `shouldBe` "\"F/S Return Match\""

--    it "Correctly parses sites from PGN notation"
--      $          site (fst . head $ parsedPGN pgnString)
--      `shouldBe` "\"Belgrade, Serbia JUG\""
--
--    it "Correctly parses dates from PGN notation"
--      $          date (fst . head $ parsedPGN pgnString)
--      `shouldBe` "\"1992.11.04\""
--
--    it "Correctly parses rounds from PGN notation"
--      $          tournRound (fst . head $ parsedPGN pgnString)
--      `shouldBe` "\"29\""
--
--    it "Correctly parses white from PGN notation"
--      $          white (fst . head $ parsedPGN pgnString)
--      `shouldBe` "\"Fischer, Robert J.\""
--
--    it "Correctly parses black from PGN notation"
--      $          black (fst . head $ parsedPGN pgnString)
--      `shouldBe` "\"Spassky, Boris V.\""
--
--    it "Correctly parses results from PGN notation"
--      $          result (fst . head $ parsedPGN pgnString)
--      `shouldBe` Draw
--
--    it "Correctly parses miscellaneous tags from PGN notation"
--      $          miscTags (fst . head $ parsedPGN pgnString)
--      `shouldBe` Map.fromList exampleMiscTags
--
--    it "Correctly parses movetext from PGN notation"
--      $          show (moves (fst . head $ parsedPGN pgnString))
--      `shouldBe` exampleMoves
 where
  pgnString = unlines
    [ "[Event \"F/S Return Match\"]"
    , "[Site \"Belgrade, Serbia JUG\"]"
    , "[Date \"1992.11.04\"]"
    , "[Round \"29\"]"
    , "[White \"Fischer, Robert J.\"]"
    , "[Black \"Spassky, Boris V.\"]"
    , "[Result \"1/2-1/2\"]"
    , "[WhiteTitle \"GM\"]"
    , "[WhiteCountry \"United States\"]"
    , ""
    , "1. e4 e5 2. Nf3 Nc6 3. Bb5 a6 4. Ba4 Nf6 5. O-O Be7 6. Re1 b5 7. Bb3 d6 8. c3"
    , "O-O 9. h3 Nb8 10. d4 Nbd7 11. c4 c6 12. cxb5 axb5 13. Nc3 Bb7 14. Bg5 b4 15."
    , "Nb1 h6 16. Bh4 c5 17. dxe5 Nxe4 18. Bxe7 Qxe7 19. exd6 Qf6 20. Nbd2 Nxd6 21."
    , "Nc4 Nxc4 22. Bxc4 Nb6 23. Ne5 Rae8 24. Bxf7+ Rxf7 25. Nxf7 Rxe1+ 26. Qxe1 Kxf7"
    , "27. Qe3 Qg5 28. Qxg5 hxg5 29. b3 Ke6 30. a3 Kd6 31. axb4 cxb4 32. Ra5 Nd5 33."
    , "f3 Bc8 34. Kf2 Bf5 35. Ra7 g6 36. Ra6+ Kc5 37. Ke1 Nf4 38. g3 Nxh3 39. Kd2 Kb5"
    , "40. Rd6 Kc5 41. Ra6 Nf2 42. g4 Bd3 43. Re6 1/2-1/2"
    ]
  exampleMiscTags =
    [("WhiteCountry", "\"United States\""), ("WhiteTitle", "\"GM\"")]
  exampleMoves
    = "[(\"e4\",\"e5\"),(\"Nf3\",\"Nc6\"),(\"Bb5\",\"a6\"),(\"Ba4\",\"Nf6\"),(\"O-O\",\"Be7\"),(\"Re1\",\"b5\"),(\"Bb3\",\"d6\"),(\"c3\",\"O-O\"),(\"h3\",\"Nb8\"),(\"d4\",\"Nbd7\"),(\"c4\",\"c6\"),(\"cxb5\",\"axb5\"),(\"Nc3\",\"Bb7\"),(\"Bg5\",\"b4\"),(\"Nb1\",\"h6\"),(\"Bh4\",\"c5\"),(\"dxe5\",\"Nxe4\"),(\"Bxe7\",\"Qxe7\"),(\"exd6\",\"Qf6\"),(\"Nbd2\",\"Nxd6\"),(\"Nc4\",\"Nxc4\"),(\"Bxc4\",\"Nb6\"),(\"Ne5\",\"Rae8\"),(\"Bxf7+\",\"Rxf7\"),(\"Nxf7\",\"Rxe1+\"),(\"Qxe1\",\"Kxf7\"),(\"Qe3\",\"Qg5\"),(\"Qxg5\",\"hxg5\"),(\"b3\",\"Ke6\"),(\"a3\",\"Kd6\"),(\"axb4\",\"cxb4\"),(\"Ra5\",\"Nd5\"),(\"f3\",\"Bc8\"),(\"Kf2\",\"Bf5\"),(\"Ra7\",\"g6\"),(\"Ra6+\",\"Kc5\"),(\"Ke1\",\"Nf4\"),(\"g3\",\"Nxh3\"),(\"Kd2\",\"Kb5\"),(\"Rd6\",\"Kc5\"),(\"Ra6\",\"Nf2\"),(\"g4\",\"Bd3\"),(\"Re6\",\"...\")]"
