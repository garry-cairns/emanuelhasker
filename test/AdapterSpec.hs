{-# LANGUAGE OverloadedStrings #-}

module AdapterSpec where

import           Test.Hspec
import           Adapters
import           GameState
import           Parsers.FEN

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  it "Correctly parses moves from movetext"
    $          deriveMoveFromText startState "e4"
    `shouldBe` Move {movement = ((5, 2), (5, 4)), promotionChoice = Nothing}

  it "Correctly parses complex moves from movetext"
    $          deriveMoveFromText twoRooksCanMove "Rhg4"
    `shouldBe` Move {movement = ((8, 4), (7, 4)), promotionChoice = Nothing}
 where
  startState =
    parsedFEN "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
  twoRooksCanMove =
    parsedFEN "2k5/4p1p1/2b3R1/p3p3/3P2rR/4P3/3KNPBB/8 w - - 0 1"
