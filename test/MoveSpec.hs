{-# LANGUAGE OverloadedStrings #-}

module MoveSpec where

import           Adapters
import           ChessLib
import qualified Data.Set                      as Set
import           GameState
import           Parsers.FEN
import           Test.Hspec

main :: IO ()
main = hspec spec


spec :: Spec
spec = describe "Moving" $ do

    -- Castling

  it "Correctly identifies a permissible castle"
    $          move readyToCastle (proposedMove ((5, 1), (7, 1)) Nothing)
    `shouldBe` parsedFEN
                 "r1bqkbnr/1ppp1ppp/p1n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 b kq - 1 4"

  it "Correctly identifies an unpermissible castle"
    $ move intermediateSquareAttacked (proposedMove ((5, 1), (7, 1)) Nothing)
    `shouldBe` intermediateSquareAttacked

  -- End castling

  -- Piece moves

  it "Correctly identifies available bishop moves"
    $          bishopMoves
                 (parsedFEN
                   "rnbqkbnr/pp2pppp/3p4/2p5/2B1P3/8/PPPP1PPP/RNBQK1NR w KQkq - 0 3"
                 )
                 (3, 4)
    `shouldBe` Set.fromList
                 [ (1, 6)
                 , (2, 3)
                 , (2, 5)
                 , (4, 3)
                 , (4, 5)
                 , (5, 2)
                 , (5, 6)
                 , (6, 1)
                 , (6, 7)
                 ]

  it "Correctly identifies available black pawn starting moves"
    $          pawnMoves
                 (parsedFEN "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1"
                 )
                 (3, 7)
    `shouldBe` Set.fromList [(3, 6), (3, 5)]

  it "Correctly identifies available black pawn continuation moves"
    $          pawnMoves
                 (parsedFEN
                   "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq - 0 2"
                 )
                 (3, 5)
    `shouldBe` Set.fromList [(3, 4)]

  it "Correctly identifies available white pawn starting moves"
    $          pawnMoves
                 (parsedFEN
                   "r1bqkbnr/pppp2pp/2n2p2/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 w KQkq - 0 4"
                 )
                 (2, 2)
    `shouldBe` Set.fromList [(2, 3), (2, 4)]

  it "Correctly identifies available white pawn continuation moves"
    $          pawnMoves
                 (parsedFEN
                   "rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2"
                 )
                 (5, 4)
    `shouldBe` Set.fromList [(5, 5)]

  it "Correctly identifies available en passant captures"
    $          pawnMoves
                 (parsedFEN
                   "rnbqkbnr/pp1pp1pp/8/2p1Pp2/8/8/PPPP1PPP/RNBQKBNR w KQkq f6 0 3"
                 )
                 (5, 5)
    `shouldBe` Set.fromList [(5, 6), (6, 6)]

  it "Correctly identifies available queen moves"
    $          queenMoves
                 (parsedFEN
                   "r1bqkbnr/pppp2pp/2n2p2/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 w KQkq - 0 4"
                 )
                 (4, 1)
    `shouldBe` Set.fromList [(5, 1), (5, 2)]

  it "Correctly identifies available rook moves"
    $          rookMoves
                 (parsedFEN
                   "r1bqkbnr/pppp2pp/2n2p2/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 w KQkq - 0 4"
                 )
                 (6, 1)
    `shouldBe` Set.fromList [(5, 1)]

  -- End piece moves

  -- Game state

  it "Can move pieces and update the game state"
    $          move sicilian (proposedMove ((7, 1), (6, 3)) Nothing)
    `shouldBe` parsedFEN
                 "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2"

  it "Can intercept illegal moves"
    $          move sicilian (proposedMove ((7, 1), (6, 2)) Nothing)
    `shouldBe` sicilian

  it "Knows when a move will leave the king in check"
    $          move dangerOfCheck (proposedMove ((4, 2), (4, 3)) Nothing)
    `shouldBe` dangerOfCheck

  it "Knows whose turn it is"
    $          rookMoves
                 (parsedFEN
                   "r1bqkbnr/pppp2pp/2n2p2/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 w KQkq - 0 4"
                 )
                 (1, 7)
    `shouldBe` Set.empty

  it "Can advance a game"
    $          advanceGame startingPosition (map uciToMove ["e2e4", "c7c5"])
    `shouldBe` sicilian

  -- End game state

  -- Promotion

  it "Can promote a pawn"
    $          move readyToPromote (proposedMove ((2, 7), (3, 8)) (Just Queen))
    `shouldBe` parsedFEN
                 "rnQqkbnr/p4ppp/3p4/2p1p3/8/8/PP1PPPPP/RNBQKBNR b KQkq - 0 4"

    -- End promotion
 where
  proposedMove mvmt pChoice = Move {movement = mvmt, promotionChoice = pChoice}
  readyToCastle = parsedFEN
    "r1bqkbnr/1ppp1ppp/p1n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq - 0 4"
  intermediateSquareAttacked =
    parsedFEN "rn1qkbnr/p1pp1ppp/bp6/4p3/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq - 0 4"
  startingPosition =
    parsedFEN "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
  sicilian =
    parsedFEN "rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2"
  dangerOfCheck =
    parsedFEN "rnbqk1nr/pppp1ppp/8/4p3/1b2P3/5N2/PPPP1PPP/RNBQKB1R w KQkq - 1 3"
  readyToPromote =
    parsedFEN "rnbqkbnr/pP3ppp/3p4/2p1p3/8/8/PP1PPPPP/RNBQKBNR w KQkq - 0 4"
