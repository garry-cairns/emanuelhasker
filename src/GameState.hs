{-# LANGUAGE OverloadedStrings #-}

module GameState
  ( GameState(..)
  , Move(..)
  , advanceGame
  , allReachableSqauresForPiece
  , bishopMoves
  , kingMoves
  , knightMoves
  , move
  , pawnMoves
  , queenMoves
  , rookMoves
  )
where

import qualified Data.Map.Strict               as Map
import qualified Data.Matrix                   as M
import           Data.Maybe                     ( fromJust
                                                , isJust
                                                )
import qualified Data.Text                     as Text
import qualified Data.Set                      as Set
import qualified ChessLib                      as CL

-- Types

data GameState = GameState
  { position         :: CL.Board
  , sideToMove       :: CL.Colour
  , availableCastles :: [CL.Castle]
  , enPassant        :: Text.Text
  , halfMove         :: Int
  , fullMove         :: Int
  }
  deriving (Eq, Show)

type Movement = (CL.Coordinate, CL.Coordinate)

data Move = NoMove | Move
  { movement        :: Movement
  , promotionChoice :: Maybe CL.ChessMan
  }
 deriving Eq
instance Show Move where
  show NoMove = "0000"
  show Move{
    movement=(start,end),
    promotionChoice=Nothing
  } = Text.unpack $ Text.concat [showCoord start, showCoord end]
  show Move{
    movement=(start,end),
    promotionChoice=p
  } = Text.unpack $ Text.concat [ showCoord start
                                , showCoord end
                                , Text.toLower $ Text.pack (show $ fromJust p)
                                ]

-- End types

-- Generic functions

showCoord :: CL.Coordinate -> Text.Text
showCoord c = CL.inverseMappedIndices Map.! c

{-|
  takeWhile but including the first element where the match fails
-}
takeUntil :: (a -> Bool) -> [a] -> [a]
takeUntil p = foldr (\x ys -> if p x then x : ys else [x]) []

-- End generic functions

-- Moving

-- -- General moving

advanceGame :: GameState -> [Move] -> GameState
advanceGame = foldl move

{-|
  Provides a set of all available moves for one side in the game that could conceivably result in a capture
-}
allAttackedSquaresForSide :: GameState -> CL.Colour -> Set.Set CL.Coordinate
allAttackedSquaresForSide gs c = foldr
  (Set.union . allReachableSqauresForPiece gs c True)
  Set.empty
  CL.chessIndices

{-|
  Provides a set of all available moves for the side not moving in the game that could conceivably result in a capture
-}
allAttackedSquaresForOtherSide :: GameState -> Set.Set CL.Coordinate
allAttackedSquaresForOtherSide gs = allAttackedSquaresForSide phonyGS
  $ sideToMove phonyGS
  -- We need this because moves when it's not your turn aren't legal
 where
  phonyGS = GameState
    { position         = position gs
    , sideToMove       = updateSideToMove (sideToMove gs)
    , availableCastles = availableCastles gs
    , enPassant        = enPassant gs
    , halfMove         = halfMove gs
    , fullMove         = fullMove gs
    }

allReachableSqauresForPiece
  :: GameState -> CL.Colour -> Bool -> CL.Coordinate -> Set.Set CL.Coordinate
allReachableSqauresForPiece gs c capturesOnly (x, y)
  |
  -- You're looking outside the board or at an empty square
    piece `elem` ["-", "."] = Set.empty
  |
  -- We are using this function as an accumulator for allAttackedSquaresForSide so pieces from the other side don't count
    CL.colour (CL.readPiece piece) /= c = Set.empty
  | piece `elem` ["b", "B"] = bishopMoves gs (x, y)
  | piece `elem` ["k", "K"] = if capturesOnly
    then kingStandardMoves gs (x, y)
    else kingMoves gs (x, y)
  | piece `elem` ["n", "N"] = knightMoves gs (x, y)
  | piece `elem` ["p", "P"] = if capturesOnly
    then pawnCaptureSquares gs (x, y)
    else pawnMoves gs (x, y)
  | piece `elem` ["q", "Q"] = queenMoves gs (x, y)
  | piece `elem` ["r", "R"] = rookMoves gs (x, y)
  | otherwise = Set.empty
  where piece = CL.occupant (position gs) (x, y)

directionalMoves
  :: GameState -> CL.Coordinate -> String -> Set.Set CL.Coordinate
directionalMoves gs (x, y) s
  | s == "forward" = getLegalMoveSet
  $ iterate (\(x1, y1) -> (x1, y1 + 1)) (x, y + 1)
  | s == "forwardRight" = getLegalMoveSet
  $ iterate (\(x1, y1) -> (x1 + 1, y1 + 1)) (x + 1, y + 1)
  | s == "right" = getLegalMoveSet
  $ iterate (\(x1, y1) -> (x1 + 1, y1)) (x + 1, y)
  | s == "backRight" = getLegalMoveSet
  $ iterate (\(x1, y1) -> (x1 + 1, y1 - 1)) (x + 1, y - 1)
  | s == "back" = getLegalMoveSet
  $ iterate (\(x1, y1) -> (x1, y1 - 1)) (x, y - 1)
  | s == "backLeft" = getLegalMoveSet
  $ iterate (\(x1, y1) -> (x1 - 1, y1 - 1)) (x - 1, y - 1)
  | s == "left" = getLegalMoveSet
  $ iterate (\(x1, y1) -> (x1 - 1, y1)) (x - 1, y)
  | s == "forwardLeft" = getLegalMoveSet
  $ iterate (\(x1, y1) -> (x1 - 1, y1 + 1)) (x - 1, y + 1)
  | otherwise = Set.empty
 where
  getLegalMoveSet =
    Set.fromList . dropIllegal gs (x, y) . reverse . takeUntil (isEmpty gs)

move :: GameState -> Move -> GameState
move gs proposedMove
  | uncurry (isLegalMove gs) coords = uncurry (unsafeMove gs maybePromotion)
                                              coords
  | otherwise = gs
 where
  coords         = movement proposedMove
  maybePromotion = promotionChoice proposedMove

unsafeMove
  :: GameState
  -> Maybe CL.ChessMan
  -> CL.Coordinate
  -> CL.Coordinate
  -> GameState
unsafeMove gs maybePromotion startCoordinate targetCoordinate = GameState
  { position = updateBoard gs maybePromotion startCoordinate targetCoordinate
  , sideToMove       = updateSideToMove (sideToMove gs)
  , availableCastles = updateCastles
    (updateBoard gs maybePromotion startCoordinate targetCoordinate)
    (availableCastles gs)
  , enPassant        = updateEnPassant gs startCoordinate targetCoordinate
  , halfMove         = updateHalfMove gs startCoordinate targetCoordinate
  , fullMove         = updateFullMove gs startCoordinate targetCoordinate
  }

-- All update functions are unsafe since they are not exported and the unsafe uses are filtered out by the caller

castle :: GameState -> CL.Coordinate -> CL.Coordinate -> CL.Board
castle gs (startx, starty) (targetx, targety)
  |
  -- Black kingside
    starty == 8 && targetx == 7 = moveTheKing
  $ updateBoard gs Nothing (8, 8) (6, 8)
  |
  -- Black queenside
    starty == 8 && targetx == 3 = moveTheKing
  $ updateBoard gs Nothing (1, 8) (4, 8)
  |
  -- White kingside
    starty == 1 && targetx == 7 = moveTheKing
  $ updateBoard gs Nothing (8, 1) (6, 1)
  |
  -- White queenside
    starty == 1 && targetx == 3 = moveTheKing
  $ updateBoard gs Nothing (1, 1) (4, 1)
  | otherwise = error "I don't know what you're doing, but it isn't castling"
 where
  newB = M.setElem CL.Empty (startx, starty)
  moveTheKing =
    M.setElem (M.getElem startx starty (position gs)) (targetx, targety) . newB

updateBoard
  :: GameState
  -> Maybe CL.ChessMan
  -> CL.Coordinate
  -> CL.Coordinate
  -> CL.Board
updateBoard gs maybePromotion (startx, starty) (targetx, targety)
  | piece `elem` ["k", "K"] && targetx `elem` [startx - 2, startx + 2] = castle
    gs
    (startx , starty)
    (targetx, targety)
  | otherwise = M.setElem newSquare (targetx, targety) newB
 where
  newB     = M.setElem CL.Empty (startx, starty) (position gs)
  piece    = CL.occupant (position gs) (startx, starty)
  newPiece = if isJust maybePromotion
    then CL.Piece (fromJust maybePromotion) (sideToMove gs)
    else CL.readPiece piece
  newSquare = CL.Filled newPiece

updateSideToMove :: CL.Colour -> CL.Colour
updateSideToMove c
  | c == CL.Black = CL.White
  | c == CL.White = CL.Black
  | otherwise = error
    "You seem to have supplied a colour that doesn't exist. Well done!"

-- Castle updating is a bit more complicated

isAvailableCastle :: CL.Board -> CL.Castle -> Bool
isAvailableCastle board c
  | c == CL.Kingside CL.Black
  = CL.occupant board (5, 8) == "k" && CL.occupant board (8, 8) == "r"
  | c == CL.Queenside CL.Black
  = CL.occupant board (5, 8) == "k" && CL.occupant board (1, 8) == "r"
  | c == CL.Kingside CL.White
  = CL.occupant board (5, 1) == "K" && CL.occupant board (8, 1) == "R"
  | c == CL.Queenside CL.White
  = CL.occupant board (5, 1) == "K" && CL.occupant board (1, 1) == "R"
  | otherwise
  = error "Bad castle in castle input"

updateCastles :: CL.Board -> [CL.Castle] -> [CL.Castle]
updateCastles board existingCastles
  | null existingCastles = []
  | otherwise            = filter (isAvailableCastle board) existingCastles

-- End castle updating

updateEnPassant :: GameState -> CL.Coordinate -> CL.Coordinate -> Text.Text
updateEnPassant gs (startx, starty) (_, targety)
  | isAtStartingPosition gs (startx, starty) && targety == starty + 2
  = CL.inverseMappedIndices Map.! (startx, starty + 1)
  | isAtStartingPosition gs (startx, starty) && targety == starty - 2
  = CL.inverseMappedIndices Map.! (startx, starty - 1)
  | otherwise
  = "-"

updateHalfMove :: GameState -> CL.Coordinate -> CL.Coordinate -> Int
updateHalfMove gs startCoordinate targetCoordinate
  |
  -- A pawn advance resets the half move counter
    CL.occupant (position gs) startCoordinate `elem` ["p", "P"] = 0
  |
  -- A capture resets the half move counter
    CL.occupant (position gs) targetCoordinate `notElem` ["-", "."] = 0
  | otherwise = halfMove gs + 1

updateFullMove :: GameState -> CL.Coordinate -> CL.Coordinate -> Int
updateFullMove gs startCoordinate (_, _)
  | CL.occupant (position gs) startCoordinate
    `elem` ["p", "n", "b", "r", "q", "k"]
  = fullMove gs + 1
  | otherwise
  = fullMove gs

-- -- End general moving

-- -- Piece movements

bishopMoves :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
bishopMoves gs (x, y) = foldr (Set.union . directionalMoves gs (x, y))
                              Set.empty
                              directions
  where directions = ["forwardLeft", "forwardRight", "backLeft", "backRight"]

kingCastlingMoves :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
kingCastlingMoves gs (x, y) =
  Set.fromList $ filter (isLegalCastle gs (x, y)) [(x - 2, y), (x + 2, y)]

kingMoves :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
kingMoves gs (x, y) =
  Set.union (kingCastlingMoves gs (x, y)) (kingStandardMoves gs (x, y))

kingStandardMoves :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
kingStandardMoves gs (x, y) = Set.fromList $ filter
  (generalLegality gs (x, y))
  [ (x    , y + 1)
  , (x + 1, y + 1)
  , (x + 1, y)
  , (x + 1, y - 1)
  , (x    , y - 1)
  , (x - 1, y - 1)
  , (x - 1, y)
  , (x - 1, y - 1)
  ]

knightMoves :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
knightMoves gs (x, y) = Set.fromList $ filter
  (generalLegality gs (x, y))
  [ (x + 2, y - 1)
  , (x + 2, y + 1)
  , (x - 2, y - 1)
  , (x - 2, y + 1)
  , (x + 1, y - 2)
  , (x + 1, y + 2)
  , (x - 1, y - 2)
  , (x - 1, y + 2)
  ]

pawnCaptureSquares :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
pawnCaptureSquares gs (x, y) = Set.fromList $ filter
  (canCapture gs (x, y))
  [(x - 1, pawnOperator y 1), (x + 1, pawnOperator y 1)]
 where
  pawnOperator =
    if CL.colour (CL.readPiece (CL.occupant (position gs) (x, y))) == CL.Black
      then (-)
      else (+)

pawnMoves :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
pawnMoves gs (x, y)
  | piece `elem` ["-", "."] = Set.empty
  | piece `elem` ["p", "P"] = Set.union (pawnCaptureSquares gs (x, y))
                                        (pawnMovingSquares gs (x, y))
  | otherwise = Set.empty
  where piece = CL.occupant (position gs) (x, y)

pawnMovingSquares :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
pawnMovingSquares gs (x, y) = if isAtStartingPosition gs (x, y)
  then Set.fromList $ takeWhile
    (isEmpty gs)
    [ (x, pawnOperator y 1)
    , (x, pawnOperator y 1)
    , (x, pawnOperator y 2)
    , (x, pawnOperator y 2)
    ]
  else Set.fromList
    $ takeWhile (isEmpty gs) [(x, pawnOperator y 1), (x, pawnOperator y 1)]
 where
  pawnOperator =
    if CL.colour (CL.readPiece (CL.occupant (position gs) (x, y))) == CL.Black
      then (-)
      else (+)

queenMoves :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
queenMoves gs (x, y) = Set.union (bishopMoves gs (x, y)) (rookMoves gs (x, y))

rookMoves :: GameState -> CL.Coordinate -> Set.Set CL.Coordinate
rookMoves gs (x, y) = foldr (Set.union . directionalMoves gs (x, y))
                            Set.empty
                            ["forward", "right", "back", "left"]

-- -- End piece movements

-- -- Conditions

canCapture :: GameState -> CL.Coordinate -> CL.Coordinate -> Bool
canCapture gs (startx, starty) (targetx, targety)
  | piece `elem` ["-", "."]
  = False
  | piece `elem` ["p", "P"] && notElem targetx [startx - 1, startx + 1]
  = False
  | piece `elem` ["p", "P"] && CL.mappedIndices Map.!? enPassant gs == Just
    (targetx, targety)
  = True
  | sameColour gs (startx, starty) (targetx, targety) == Just False
  = True
  | otherwise
  = False
  where piece = CL.occupant (position gs) (startx, starty)

dropIllegal :: GameState -> CL.Coordinate -> [CL.Coordinate] -> [CL.Coordinate]
dropIllegal gs startCoordinate (x : xs)
  | null (x : xs)
  = []
  | generalLegality gs startCoordinate x
  = x : filter (generalLegality gs startCoordinate) xs
  | otherwise
  = filter (generalLegality gs startCoordinate) xs

isAttacked :: GameState -> Set.Set CL.Coordinate -> Bool
isAttacked gs coOrdinateSet =
  Set.intersection coOrdinateSet (allAttackedSquaresForOtherSide gs)
    /= Set.empty

isAtStartingPosition :: GameState -> CL.Coordinate -> Bool
isAtStartingPosition gs (file, rank)
  | piece == "k" && (file, rank) == (5, 8) = True
  | piece == "K" && (file, rank) == (5, 1) = True
  | piece == "p" && rank == 7              = True
  | piece == "P" && rank == 2              = True
  | piece == "r" && (file, rank) `elem` [(1, 8), (8, 8)] = True
  | piece == "R" && (file, rank) `elem` [(1, 1), (8, 1)] = True
  | otherwise                              = False
  where piece = CL.occupant (position gs) (file, rank)

isEmpty :: GameState -> CL.Coordinate -> Bool
isEmpty gs (targetx, targety)
  |
  -- If the target square doesn't exist it's not "empty"
    CL.occupant (position gs) (targetx, targety) == "." = True
  | otherwise = False

isKingInCheck :: GameState -> CL.Colour -> Bool
isKingInCheck gs c =
  Set.intersection (CL.kingPosition (position gs) c)
                   (allAttackedSquaresForOtherSide gs)
    /= Set.empty

isLegalCastle :: GameState -> CL.Coordinate -> CL.Coordinate -> Bool
isLegalCastle gs (startx, starty) (targetx, targety)
  |
  -- No piece no castle
    piece `elem` ["-", "."]
  = False
  |
  -- No King no castle
    CL.piece (CL.readPiece piece) /= CL.King
  = False
  |
  -- King has moved so no castle
    not $ isAtStartingPosition gs (startx, starty)
  = False
  |
  -- No castles are valid so no castle
    null $ availableCastles gs
  = False
  |
  -- King is in check or would have to move through a checked square so no castle
    isAttacked gs $ Set.fromList
    [ (startx                 , starty)
    , (castleOperator startx 1, starty)
    , (castleOperator startx 2, starty)
    ]
  = False
  |
  -- Things in the way so no castle
    CL.occupant (position gs) (castleOperator startx 1, starty) /= "."
  = False
  | CL.occupant (position gs) (castleOperator startx 2, starty) /= "."
  = False
  |
  -- The specific castle you want to try is no longer valid so no castle
    (startx, starty)
    ==        (5      , 8)
    &&        (targetx, targety)
    ==        (7      , 8)
    &&        CL.Kingside CL.Black
    `notElem` availableCastles gs
  = False
  | (startx, starty)
    ==        (5      , 8)
    &&        (targetx, targety)
    ==        (3      , 8)
    &&        CL.Queenside CL.Black
    `notElem` availableCastles gs
  = False
  | (startx, starty)
    ==        (5      , 1)
    &&        (targetx, targety)
    ==        (7      , 1)
    &&        CL.Kingside CL.White
    `notElem` availableCastles gs
  = False
  | (startx, starty)
    ==        (5      , 1)
    &&        (targetx, targety)
    ==        (3      , 1)
    &&        CL.Queenside CL.White
    `notElem` availableCastles gs
  = False
  | otherwise
  = True
 where
  castleOperator = if targetx < startx then (-) else (+)
  piece          = CL.occupant (position gs) (startx, starty)

isLegalMove :: GameState -> CL.Coordinate -> CL.Coordinate -> Bool
isLegalMove gs startCoordinate targetCoordinate
  |
  -- If the proposed move leaves the side moving's King in check it's not a legal move. Checking here to avoid infinite recursion.
    isKingInCheck phonyGS (sideToMove gs)
  = False
  |
  -- If the moving piece can't reach the square it's a legal move
    targetCoordinate
    `notElem` allReachableSqauresForPiece gs
                                          (CL.colour (CL.readPiece piece))
                                          False
                                          startCoordinate
  = False
  | otherwise
  = True
 where
  phonyGS = GameState
    { position         = updateBoard gs Nothing startCoordinate targetCoordinate
    , sideToMove       = sideToMove gs
    , availableCastles = availableCastles gs
    , enPassant        = enPassant gs
    , halfMove         = halfMove gs
    , fullMove         = fullMove gs
    }
  piece = CL.occupant (position gs) startCoordinate

generalLegality :: GameState -> CL.Coordinate -> CL.Coordinate -> Bool
generalLegality gs startCoordinate targetCoordinate
  |
  -- If the start square is off the board or empty it's not a legal move
    startSquare `elem` ["-", "."]       = False
  |
  -- If the start and target co-ordinates are the same it's not a legal move
    startCoordinate == targetCoordinate = False
  |
  -- If it's not your turn it's not a legal move
    CL.colour (CL.readPiece startSquare) /= sideToMove gs = False
  |
  -- If the target square is occupied then only a capture is legal
    targetSquare /= "." = canCapture gs startCoordinate targetCoordinate
  | otherwise                           = True
 where
  startSquare  = CL.occupant (position gs) startCoordinate
  targetSquare = CL.occupant (position gs) targetCoordinate

sameColour :: GameState -> CL.Coordinate -> CL.Coordinate -> Maybe Bool
sameColour gs startCoordinate targetCoordinate
  | piece `elem` ["-", "."] = Nothing
  | target `elem` ["-", "."] = Nothing
  | CL.colour (CL.readPiece piece) == CL.colour (CL.readPiece target) = Just
    True
  | otherwise = Just False
 where
  piece  = CL.occupant (position gs) startCoordinate
  target = CL.occupant (position gs) targetCoordinate

-- -- End conditions

-- End moving
