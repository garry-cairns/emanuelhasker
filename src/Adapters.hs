{-# LANGUAGE OverloadedStrings #-}

module Adapters
  ( deriveMoveFromText
  , uciToMove
  )
where

import qualified Data.Text                     as Text
import qualified Data.Map.Strict               as Map
import           ChessLib
import qualified GameState                     as GS

-- General funcs

allSubstrings :: Int -> Text.Text -> [Text.Text]
allSubstrings n s
  | Text.length s >= n = Text.take n s : allSubstrings n (Text.tail s)
  | otherwise          = []

coordinatesInText :: Text.Text -> [Coordinate]
coordinatesInText = map (mappedIndices Map.!) . squareAddressesInText

squareAddressesInText :: Text.Text -> [Text.Text]
squareAddressesInText s =
  [ address
  | address <- allSubstrings 2 filteredText
  , Text.head address `elem` ['a' .. 'h']
  , Text.index address 1 `elem` ['1' .. '8']
  ]
  where filteredText = filterPGNText s

-- End general funcs

-- PGN to Move

-- |Creates a Move record type from an individual PGN move string such as "e4"
deriveMoveFromText :: GS.GameState -> Text.Text -> GS.Move
deriveMoveFromText gs s = GS.Move
  { GS.movement        = (pgnTextToSource s gs, pgnTextToDest s gs)
  , GS.promotionChoice = pgnTextToChessMan s
  }

filterPGNText :: Text.Text -> Text.Text
filterPGNText = Text.filter (`notElem` filterText)
  where filterText = "x+!?#" :: String

kingStartingRank :: Colour -> Int
kingStartingRank c | c == Black = 8
                   | c == White = 1
                   | otherwise  = error "No colour supplied"

pgnCharToPiece :: Char -> Colour -> Piece
pgnCharToPiece c col
  | c `elem` ['a' .. 'h']     = Piece {piece = Pawn, colour = col}
  | c == 'N'                  = Piece {piece = Knight, colour = col}
  | c == 'B'                  = Piece {piece = Bishop, colour = col}
  | c == 'R'                  = Piece {piece = Rook, colour = col}
  | c == 'Q'                  = Piece {piece = Queen, colour = col}
  | c `elem` ("KO" :: String) = Piece {piece = King, colour = col}
  | otherwise                 = error errorText
  where errorText = "You have supplied Movetext that doesn't seem valid"

pgnTextToChessMan :: Text.Text -> Maybe ChessMan
pgnTextToChessMan s | Text.isInfixOf "=N" s = Just Knight
                    | Text.isInfixOf "=B" s = Just Bishop
                    | Text.isInfixOf "=R" s = Just Rook
                    | Text.isInfixOf "=Q" s = Just Queen
                    | otherwise             = Nothing

pgnTextToDest :: Text.Text -> GS.GameState -> Coordinate
pgnTextToDest s gs | s == "O-O-O" = (3, kingStartingRank $ GS.sideToMove gs)
                   | s == "O-O" = (7, kingStartingRank $ GS.sideToMove gs)
                   | length explicitCoordinates == 1 = head explicitCoordinates
                   | length explicitCoordinates == 2 = last explicitCoordinates
                   | otherwise = error errorText
 where
  explicitCoordinates = coordinatesInText s
  errorText           = "Could not infer a destination from the text given"

pgnTextToPartialSquare :: Text.Text -> Char
pgnTextToPartialSquare xs | length twoFiles == 1 = Text.head $ head twoFiles
                          | length twoRanks == 1 = Text.head $ head twoRanks
                          | otherwise            = error errorText
 where
  errorText    = "Text supplied can't be converted into a chess move"
  filteredText = filterPGNText xs
  twoFiles =
    [ address
    | address <- allSubstrings 3 filteredText
    , Text.head address `elem` ['a' .. 'h']
    , Text.index address 1 `elem` ['a' .. 'h']
    , Text.index address 2 `elem` ['1' .. '8']
    ]
  twoRanks =
    [ address
    | address <- allSubstrings 3 filteredText
    , Text.head address `elem` ['1' .. '8']
    , Text.index address 1 `elem` ['a' .. 'h']
    , Text.index address 2 `elem` ['1' .. '8']
    ]

pgnTextToSource :: Text.Text -> GS.GameState -> Coordinate
pgnTextToSource s gs
  | s == "O-O-O"                    = (5, kingStartingRank $ GS.sideToMove gs)
  | s == "O-O"                      = (5, kingStartingRank $ GS.sideToMove gs)
  | length explicitCoordinates == 2 = head explicitCoordinates
  | length implicitCoordinates == 1 = head implicitCoordinates
  | otherwise = resolveAmbiguousMoves s implicitCoordinates
 where
  explicitCoordinates = coordinatesInText s
  implicitCoordinates = potentialSourceSquares s gs

potentialSourceSquares :: Text.Text -> GS.GameState -> [Coordinate]
potentialSourceSquares t gs =
  [ c
  | c <- chessIndices
  , occupant (GS.position gs) c == movingPiece
  , destination `elem` reachable c
  ]
 where
  movingPiece =
    Text.pack . show . pgnCharToPiece (Text.head t) $ GS.sideToMove gs
  destination = pgnTextToDest t gs
  reachable   = GS.allReachableSqauresForPiece gs (GS.sideToMove gs) False

resolveAmbiguousMoves :: Text.Text -> [Coordinate] -> Coordinate
resolveAmbiguousMoves t coords | length choices == 1 = head choices
                               | otherwise           = error errorText
 where
  squareAddresses = zip (map (inverseMappedIndices Map.!) coords) coords
  choices =
    [ snd c
    | c <- squareAddresses
    , pgnTextToPartialSquare t `elem` (Text.unpack (fst c))
    ]
  errorText = "Cannot resolve ambiguous move text"

-- End PGN to Move

-- UCI to Move; see http://wbec-ridderkerk.nl/html/UCIProtocol.html

charToChessMan :: Char -> Maybe ChessMan
charToChessMan c | c == 'n'  = Just Knight
                 | c == 'b'  = Just Bishop
                 | c == 'r'  = Just Rook
                 | c == 'q'  = Just Queen
                 | otherwise = Nothing

isValidUCI :: Text.Text -> Bool
isValidUCI t
  | Text.length t == 4 = enoughCoords
  | Text.length t == 5 = enoughCoords && Text.last t `elem` ['n', 'b', 'r', 'q']
  | otherwise          = False
  where enoughCoords = length (coordinatesInText t) == 2

uciToMove :: Text.Text -> GS.Move
uciToMove t
  | not $ isValidUCI t = GS.NoMove
  | Text.length t `elem` [4, 5] = GS.Move
    { GS.movement        = (head explicitCoordinates, last explicitCoordinates)
    , GS.promotionChoice = charToChessMan $ Text.last t
    }
  | otherwise = GS.NoMove
  where explicitCoordinates = coordinatesInText t

-- End UCI to Move
