{-# LANGUAGE OverloadedStrings #-}

module ChessLib
  ( Board
  , Castle(..)
  , ChessMan(..)
  , Colour(..)
  , Coordinate
  , Piece(..)
  , Square(..)
  , boardToFENText
  , chessIndices
  , inverseMappedIndices
  , kingPosition
  , mappedIndices
  , occupant
  , readPiece
  )
where

import qualified Data.Text                     as Text
import qualified Data.Map.Strict               as Map
import qualified Data.Matrix                   as M
import           Data.Maybe                     ( isNothing )
import qualified Data.Set                      as Set

-- Board

type Board = M.Matrix Square

type Coordinate = (Int, Int)

data Square = Empty | Filled Piece
  deriving Eq
instance Show Square where
  show Empty           = "."
  show (Filled p)      = show p

{-|
  Generates a list of the standard algebraic chess notation square names a1-h8
-}
algebraicIndices :: [Text.Text]
algebraicIndices =
  [ Text.pack [file, rank] | file <- ['a' .. 'h'], rank <- ['1' .. '8'] ]

{-|
  Generates a list of tuples representing the matrix co-ordinates of all sqaures on a chess board
-}
chessIndices :: [Coordinate]
chessIndices = [ (file, rank) | file <- [1 .. 8], rank <- [1 .. 8] ]

{-|
  Maps algebraic chess notation to matrix co-ordinates, for example "e4": (5, 4)
-}
mappedIndices :: Map.Map Text.Text Coordinate
mappedIndices = Map.fromList (zip algebraicIndices chessIndices)

{-|
  Maps matrix co-ordinates to algebraic indices, for example (5, 4): "e4"
-}
inverseMappedIndices :: Map.Map Coordinate Text.Text
inverseMappedIndices = Map.fromList (zip chessIndices algebraicIndices)

{-|
  Returns King co-ordinates for the given side
-}
kingPosition :: Board -> Colour -> Set.Set Coordinate
kingPosition b c = Set.fromList
  [ (x, y) | (x, y) <- kingPositions b, colourMatches (occupant b (x, y)) ]
 where
  colourMatches king | c == White = king == "K"
                     | c == Black = king == "k"
                     | otherwise  = False

{-|
  Returns both King co-ordinates
-}
kingPositions :: Board -> [Coordinate]
kingPositions b =
  [ (x, y) | (x, y) <- chessIndices, occupant b (x, y) `elem` ["K", "k"] ]

{-|
  Extracts piece information from a Square
-}
occupant :: Board -> Coordinate -> Text.Text
occupant b (x, y) | isNothing (M.safeGet x y b) = "-"
                  | otherwise = Text.pack $ show (M.getElem x y b)

-- End board

-- Pieces

data Colour = Black | White
  deriving (Eq, Show)

data ChessMan = Pawn | Knight | Bishop | Rook | Queen | King
  deriving (Eq, Show)

data Piece = Piece
  { piece :: ChessMan
  , colour :: Colour
  }
  deriving Eq
instance Show Piece where
  show (Piece Pawn Black)   = "p"
  show (Piece Pawn White)   = "P"
  show (Piece Knight Black) = "n"
  show (Piece Knight White) = "N"
  show (Piece Bishop Black) = "b"
  show (Piece Bishop White) = "B"
  show (Piece Rook Black)   = "r"
  show (Piece Rook White)   = "R"
  show (Piece Queen Black)  = "q"
  show (Piece Queen White)  = "Q"
  show (Piece King Black)   = "k"
  show (Piece King White)   = "K"

readPiece :: Text.Text -> Piece
readPiece s | s == "p"  = Piece Pawn Black
            | s == "P"  = Piece Pawn White
            | s == "n"  = Piece Knight Black
            | s == "N"  = Piece Knight White
            | s == "b"  = Piece Bishop Black
            | s == "B"  = Piece Bishop White
            | s == "r"  = Piece Rook Black
            | s == "R"  = Piece Rook White
            | s == "q"  = Piece Queen Black
            | s == "Q"  = Piece Queen White
            | s == "k"  = Piece King Black
            | s == "K"  = Piece King White
            | otherwise = error "That's not a piece"

-- End pieces

-- Game logic

data Castle =  Kingside Colour | Queenside Colour | NoCastle
  deriving Eq
instance Show Castle where
  show NoCastle          = "-"
  show (Kingside Black)  = "k"
  show (Kingside White)  = "K"
  show (Queenside Black) = "q"
  show (Queenside White) = "Q"

-- End game logic

-- encoders

{-|
  Takes a position board where a1 = (1, 1) and h8 = (8, 8) and orders it such that FEN strings expect
-}
orderPosition :: Board -> [[Square]]
orderPosition m =
  M.toLists . M.transpose . M.fromLists . map reverse $ M.toLists m

{-|
  Takes a list of Squares and provides a FEN string representation of a board rank
-}
toFENRank :: [Square] -> Text.Text
toFENRank [] = ""
toFENRank (x : xs)
  | x /= Empty = Text.concat [Text.pack (show x), toFENRank xs]
  | otherwise = Text.concat
    [ Text.pack . show . length . takeWhile (== Empty) $ (x : xs)
    , toFENRank . dropWhile (== Empty) $ (x : xs)
    ]

boardToFENText :: Board -> Text.Text
boardToFENText m = Text.intercalate "/" . map toFENRank $ orderPosition m

-- END encoders
