{- |
Module      :  $Header$
Description :  Parses chess portable game notation (PGN) files
Copyright   :  (c) Garry Cairns, 2018
License     :  BSD3

Maintainer  :  garryjcairns@gmail.com
Stability   :  experimental
Portability :  portable
-}
{-# LANGUAGE OverloadedStrings #-}

module Parsers.PGN
  ( Game(..)
  , Result(..)
  , parsePGN
  , parsedPGN
  )
where

import           Prelude                 hiding ( unwords
                                                , words
                                                )
import           Data.Either                    ( fromRight
                                                , rights
                                                )
import qualified Data.Map.Strict               as Map
import           Data.Text                      ( Text
                                                , pack
                                                , unwords
                                                )
import           Data.Void                      ( Void )
import           Text.Megaparsec                ( Parsec
                                                , choice
                                                , many
                                                , manyTill
                                                , parse
                                                , some
                                                , (<|>)
                                                )
import           Text.Megaparsec.Char           ( asciiChar
                                                , char
                                                , digitChar
                                                , space
                                                , string
                                                )
import           Parsers.FEN                    ( parseFEN )
import           GameState                      ( GameState(..) )

-- Types

type Parser = Parsec Void Text

-- |Sum type denoting the possible outcomes of a game of chess
data Result = WhiteWin
  | BlackWin
  | Draw
  deriving (Eq, Show)

{-|
Record type describing a game of chess. The fields are derived from the
mandatory tags from the PGN specification
(http://www.saremba.de/chessgml/standards/pgn/pgn-complete.htm);
a startingState field to allow for PGNs that start from a non-standard
position; plus a section to contain any additional tags in the PGN.
-}
data Game = Game
  { event         :: Text
  , site          :: Text
  , date          :: Text
  , tournRound    :: Text
  , white         :: Text
  , black         :: Text
  , result        :: Result
  , startingState :: GameState
  , moves         :: [Text]
  , miscTags      :: Map.Map Text Text
  }
  deriving (Eq, Show)

-- End types

-- general helpers

-- |Converts the standard string representations of chess results to the Result type
textToResult :: Text -> Result
textToResult s | s == "1-0" = WhiteWin
               | s == "0-1" = BlackWin
               | otherwise  = Draw

-- End general helpers

-- parse helpers

-- |Matches the closing bracket of a PGN tag
closingTag :: Parser Char
closingTag = char ']'

-- |Matches the result text from a PGN
gameResult :: Parser Text
gameResult = string "1-0" <|> string "1/2-1/2" <|> string "0-1"

-- |Matches the opening bracket of a PGN tag
openingTag :: Parser Char
openingTag = char '['

parseMove :: Parser Text
parseMove = do
  _ <- some digitChar
  _ <- char '.'
  space
  plies <- some parsePly
  return (unwords plies)

parsePly :: Parser Text
parsePly = do
  start <- choice
    [ char 'a'
    , char 'b'
    , char 'c'
    , char 'd'
    , char 'e'
    , char 'f'
    , char 'g'
    , char 'h'
    , char 'B'
    , char 'K'
    , char 'N'
    , char 'O'
    , char 'Q'
    , char 'R'
    ]
  ply <- many allowable
  space
  return (pack (start : ply))
 where
  allowable = choice
    [ char 'a'
    , char 'b'
    , char 'c'
    , char 'd'
    , char 'e'
    , char 'f'
    , char 'g'
    , char 'h'
    , char 'x'
    , char 'B'
    , char 'K'
    , char 'N'
    , char 'O'
    , char 'Q'
    , char 'R'
    , char '1'
    , char '2'
    , char '3'
    , char '4'
    , char '5'
    , char '6'
    , char '7'
    , char '8'
    , char '-'
    , char '+'
    , char '#'
    , char '!'
    , char '?'
    ]

parseMoves :: Parser [Text]
parseMoves = manyTill parseMove gameResult

parseTags :: Parser (Map.Map Text Text)
parseTags = do
  tags <- manyTill pgnStringTag (char '\n')
  return (Map.fromList tags)

pgnStringTag :: Parser (Text, Text)
pgnStringTag = do
  _        <- openingTag
  tagType  <- manyTill asciiChar (char ' ')
  tagValue <- manyTill asciiChar closingTag
  _        <- char '\n'
  -- return (pack tagType, pack tagValue)
  return (pack tagType, pack tagValue)

-- END parse helpers

-- parser

parsePGN :: Parser Game
parsePGN = do
  tags     <- parseTags
  moveText <- parseMoves
  space
  return Game
    { event         = tags Map.! "Event"
    , site          = tags Map.! "Site"
    , date          = tags Map.! "Date"
    , tournRound    = tags Map.! "Round"
    , white         = tags Map.! "White"
    , black         = tags Map.! "Black"
    , result        = textToResult $ tags Map.! "Result"
    , miscTags      = fst $ notMand tags
    , startingState = if Map.member "FEN" tags
      then fromRight (error "FEN parse error")
                     (parse parseFEN "" (tags Map.! "FEN"))
      else fromRight (error "FEN parse error") (parse parseFEN "" standard)
    , moves         = moveText
    }
 where
  notMand = Map.partitionWithKey
    (\k _ ->
      k `notElem` ["Event", "Site", "Date", "Round", "White", "Black", "Result"]
    )
  standard = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

-- |Parser for PGN string(s)
parsedPGN :: Text -> [Game]
parsedPGN t = rights [parse parsePGN "" t]

-- END parser

