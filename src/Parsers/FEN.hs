{-# OPTIONS_GHC -fno-warn-type-defaults #-}

module Parsers.FEN
  ( isDigit'
  , parseFEN
  , parsedFEN
  )
where

import           Data.Either                    ( rights )
import qualified Data.List.Split               as Split
import qualified Data.Matrix                   as M
import           Data.Text                      ( Text
                                                , pack
                                                )
import           Data.Void                      ( Void )
import           Text.Megaparsec                ( Parsec
                                                , parse
                                                , satisfy
                                                , some
                                                , (<|>)
                                                )
import           Text.Megaparsec.Char           ( char
                                                , digitChar
                                                , space
                                                )
import           ChessLib
import           GameState                      ( GameState(..) )

type Parser = Parsec Void Text

-- parse helpers

charToColour :: Char -> Colour
charToColour 'b' = Black
charToColour 'w' = White
charToColour _   = undefined

charToCastle :: Char -> Castle
charToCastle 'k' = Kingside Black
charToCastle 'K' = Kingside White
charToCastle 'q' = Queenside Black
charToCastle 'Q' = Queenside White
charToCastle _   = NoCastle

{-|
  Takes standard FEN representations of pieces and Types them appropriately
-}
charToSquare :: Char -> Square
charToSquare 'p' = Filled (Piece Pawn Black)
charToSquare 'P' = Filled (Piece Pawn White)
charToSquare 'n' = Filled (Piece Knight Black)
charToSquare 'N' = Filled (Piece Knight White)
charToSquare 'b' = Filled (Piece Bishop Black)
charToSquare 'B' = Filled (Piece Bishop White)
charToSquare 'r' = Filled (Piece Rook Black)
charToSquare 'R' = Filled (Piece Rook White)
charToSquare 'q' = Filled (Piece Queen Black)
charToSquare 'Q' = Filled (Piece Queen White)
charToSquare 'k' = Filled (Piece King Black)
charToSquare 'K' = Filled (Piece King White)
charToSquare _   = Empty

fenAvailableCastles :: Parser [Castle]
fenAvailableCastles = do
  castles <- some isFenAvailableCastle
  space
  return (map charToCastle castles)

fenEnPassant :: Parser Text
fenEnPassant = do
  ep <- some isFenEnPassant
  space
  return (pack ep)

fenHalfMove :: Parser Int
fenHalfMove = do
  digit <- some isDigit'
  space
  return (read digit)

fenFullMove :: Parser Int
fenFullMove = do
  digit <- some isDigit'
  return (read digit)

fenNextMove :: Parser Colour
fenNextMove = do
  clr <- char 'b' <|> char 'w'
  space
  return (charToColour clr)

fenPosition :: Parser Board
fenPosition = do
  pos <- some isFenPositionChar
  space
  return (stringToBoard pos)

isDigit' :: Parser Char
isDigit' = digitChar

isFenAvailableCastle :: Parser Char
isFenAvailableCastle =
  char 'K' <|> char 'Q' <|> char 'k' <|> char 'q' <|> char '-'

isFenEnPassant :: Parser Char
isFenEnPassant = satisfy (`elem` "abcdefgh36-")

isFenPositionChar :: Parser Char
isFenPositionChar = satisfy (`elem` "rnbqkpPRNBQK12345678/")

fillSquares :: Char -> [Square]
fillSquares c | c `elem` ['1' .. '8'] = [ Empty | _ <- [1 .. read [c]] ]
              | otherwise             = [charToSquare c]

makeRow :: String -> [Square]
makeRow = concatMap fillSquares

{-|
  Generates an 8x8 matrix representing a chess board in a given position
-}
stringToBoard :: String -> Board
stringToBoard s =
  M.transpose . M.fromLists . map makeRow . reverse $ Split.splitOn "/" s

-- END parse helpers

-- parser

parseFEN :: Parser GameState
parseFEN = do
  pos       <- fenPosition
  toMove    <- fenNextMove
  avCastles <- fenAvailableCastles
  enPass    <- fenEnPassant
  halfM     <- fenHalfMove
  GameState pos toMove avCastles enPass halfM <$> fenFullMove

-- |Parser for FEN string(s)
parsedFEN :: Text -> GameState
parsedFEN t = head $ rights [parse parseFEN "" t]

-- END parser
