let

pkgs = import <nixpkgs> { };

in
{ EmanuelHasker = pkgs.haskellPackages.callPackage ./default.nix { };
}
