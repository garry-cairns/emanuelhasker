{ mkDerivation, base, containers, gloss, hspec, hspec-discover
, hspec-megaparsec, lens, matrix, megaparsec, process, QuickCheck
, split, stdenv, text, time
}:
mkDerivation {
  pname = "EmanuelHasker";
  version = "0.1.0.0";
  src = ../.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base containers lens matrix megaparsec split text time
  ];
  executableHaskellDepends = [ base gloss megaparsec process text ];
  testHaskellDepends = [
    base containers hspec hspec-discover hspec-megaparsec megaparsec
    QuickCheck split text
  ];
  testToolDepends = [ hspec-discover ];
  homepage = "https://ilikewhenit.works";
  description = "A simple chess program";
  license = stdenv.lib.licenses.bsd3;
}
